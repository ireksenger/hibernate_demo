package com.javagda18;

import javafx.scene.chart.XYChart;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor

public class Book extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;  // AUTO_INCREMENT to to samo co GenerrateValue
    private String tytul;
    private String autorOfBook;
    private LocalDate publicationDate;
    private BookType bookType;
    private int iloscStron;


    @OneToOne
    private Autor autor;

    public Book(Long id, String tytul, String autorOfBook, LocalDate publicationDate, BookType bookType, int iloscStron) {
        this.id = id;
        this.tytul = tytul;
        this.autorOfBook = autorOfBook;
        this.publicationDate = publicationDate;
        this.bookType = bookType;
        this.iloscStron = iloscStron;
    }
}
