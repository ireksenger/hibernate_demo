package com.javagda18;

import javafx.scene.chart.XYChart;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Autor extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;  // AUTO_INCREMENT to to samo co GenerrateValue
    private String name;
    private String  surrname;
    private LocalDate birtDay;
    private String birthPlace;

    // jeden teacher posada jednego studenta
    @OneToMany(mappedBy = "autor", fetch = FetchType.EAGER)
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<Book> student= new ArrayList<>();

    public Autor(Long id, String name, String surrname, LocalDate birtDay, String birthPlace) {
        this.id = id;
        this.name = name;
        this.surrname = surrname;
        this.birtDay = birtDay;
        this.birthPlace = birthPlace;
    }


}
