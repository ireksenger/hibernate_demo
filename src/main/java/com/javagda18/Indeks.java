package com.javagda18.model;


import com.javagda18.Book;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Indeks {
    private String bookIndeks;
    private Book book;


}
