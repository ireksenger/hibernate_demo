package com.javagda18;

import lombok.extern.java.Log;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.QueryHint;
import javax.persistence.RollbackException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
@Log

public class Dao {
    // Data Access Oject - dostęp do danych z bazy
    public static void seveEntity(BaseEntity baseEntity) {
        // pobieramy fabrykę (do tworzenia sesji)
        SessionFactory factory = HibernateUtil.getSessionFactory();

        // tworzymy sesję
        Transaction transaction = null;
        try (Session session = factory.openSession()) {


            // tworzę (rozpoczynam) transakcję
            transaction = session.beginTransaction();

            // zapis obiektu
            session.saveOrUpdate(baseEntity);

            // commit
            transaction.commit();
        } catch (HibernateException | RollbackException e) {
            log.log(Level.SEVERE, "ERROR SAVING OBJECT.");
            if (transaction != null) {
                transaction.rollback();
            }
        }

    }

    public static <T> List<T> getAll(Class<T> tClass) {

        // pobieramy fabrykę (do tworzenia sesji)
        SessionFactory factory = HibernateUtil.getSessionFactory();
        try (Session session = factory.openSession()) {
            String nazwaKlasy = tClass.getCanonicalName();
            Query<T> query = session.createQuery("from "+nazwaKlasy+" o", tClass);
// zapytanie o obiekt
            List<T> objectList = query.list();
            return objectList;

        } catch (HibernateException he) {
            log.log(Level.SEVERE, "Error loadnig objects.");
        }
        return new ArrayList<>();

    }
}

