package com.javagda18;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class Main {


    public static void main(String[] args) {
//
//
        Book book = new Book (null, "W pustyni i w puszczy", "Adam Mickiewicz",LocalDate.now(), BookType.AKCJI, 20);
        Book book1 = new Book (null, "Zła książka", "Dawid Nowak",LocalDate.now(), BookType.PRZYGOTOWA, 200);
        Autor autor = new Autor(null,"Karol", "Janosik", LocalDate.ofYearDay(2000,01), "Katowice");


        // Tworzenie realacji między obiektami
        // 1. zapis obu encji.
        Dao.seveEntity(autor);
        Dao.seveEntity(book);
        Dao.seveEntity(book1);


        // 2 powiązanie realcji
        autor.getBook().add(book);
        autor.getBook().add(book1);
        book.setAutor(autor);
        book1.setAutor(autor);


        // 3 zais encji
        Dao.seveEntity(autor);
        Dao.seveEntity(book);
        Dao.seveEntity(book1);


        List<Book> bookList= Dao.getAll(Book.class);
        System.out.println(bookList);

        List<Autor> autorList = Dao.getAll(Autor.class);
        System.out.println(autorList);
        Autor autor1 = autorList.get(0);
        for (Book nazwa : autor1.getBook()) {
            System.out.println(nazwa);

    /*       List<Student> studentList = Dao.getAll(Student.class);
            System.out.println(studentList);

            List<Teacher> teacherList = Dao.getAll(Teacher.class);
            System.out.println(teacherList);
            Teacher teacher1 = teacherList.get(0);
            for (Student nazwa : teacher1.getStudent()) {
                System.out.println(nazwa);*/

        }
    }
}



